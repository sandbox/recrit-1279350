<?php

/**
 * Implements hook_entity_info_alter().
 */
function xmlsitemap_commerce_product_entity_info_alter(array &$info) {
  if (isset($info['commerce_product'])) {
    if (!isset($info['commerce_product']['bundle label'])) {
      $info['commerce_product']['bundle label'] = t('Commerce Product');
    }

    $info['commerce_product']['xmlsitemap'] = array(
      'process callback' => 'xmlsitemap_commerce_product_xmlsitemap_process_product_links',
    );
  }
}

/**
 * Implements hook_cron().
 *
 * Process old products not found in the {xmlsitemap} table.
 */
function xmlsitemap_commerce_product_cron() {
  xmlsitemap_commerce_product_xmlsitemap_index_links(xmlsitemap_var('batch_limit'));
}

/**
 * Implements hook_xmlsitemap_index_links().
 */
function xmlsitemap_commerce_product_xmlsitemap_index_links($limit) {
  if ($types = xmlsitemap_get_link_type_enabled_bundles('commerce_product')) {
    $pids = db_query_range("SELECT p.product_id FROM {commerce_product} p LEFT JOIN {xmlsitemap} x ON x.type = 'commerce_product' AND p.product_id = x.id WHERE x.id IS NULL AND p.type IN (:types) ORDER BY p.product_id DESC", 0, $limit, array(':types' => $types))->fetchCol();
    xmlsitemap_commerce_product_xmlsitemap_process_product_links($pids);
  }
}

/**
 * Process product sitemap links.
 *
 * @param $pids
 *   An array of product IDs.
 */
function xmlsitemap_commerce_product_xmlsitemap_process_product_links(array $pids) {
  $products = commerce_product_load_multiple($pids);
  foreach ($products as $product) {
    $link = xmlsitemap_commerce_product_create_link($product);
    xmlsitemap_link_save($link);
  }
}

/**
 * Implements hook_commerce_product_insert().
 */
function xmlsitemap_commerce_product_commerce_product_insert(stdClass $product) {
  xmlsitemap_commerce_product_commerce_product_update($product);
}

/**
 * Implements hook_commerce_product_update().
 */
function xmlsitemap_commerce_product_commerce_product_update(stdClass $product) {
  $link = xmlsitemap_commerce_product_create_link($product);
  xmlsitemap_link_save($link);
}

/**
 * Implements hook_commerce_product_delete().
 */
function xmlsitemap_commerce_product_commerce_product_delete(stdClass $product) {
  xmlsitemap_link_delete('commerce_product', $product->product_id);
}

/**
 * Implements hook_field_extra_fields().
 */
function xmlsitemap_commerce_product_field_extra_fields() {
  $extras = array('commerce_product' => array());
  $enabled_bundles = xmlsitemap_get_link_type_enabled_bundles('commerce_product');

  foreach (commerce_product_type_get_name() as $type => $name) {
    if (!in_array($type, $enabled_bundles)) {
      continue;
    }

    $extras['commerce_product'][$type] = array(
      'form' => array(
        'xmlsitemap' => array(
          'label' => t('XML sitemap'),
          'description' => t('XML sitemap module element'),
          'weight' => 30,
        ),
      ),
    );
  }

  return $extras;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @see commerce_product_ui_product_type_form()
 * @see xmlsitemap_add_link_bundle_settings()
 */
function xmlsitemap_commerce_product_form_commerce_product_ui_product_type_form_alter(&$form, $form_state) {
  module_load_include('inc', 'xmlsitemap', 'xmlsitemap.admin');
  $type = isset($form_state['product_type']['type']) ? $form_state['product_type']['type'] : '';

  $submit_handlers_before = $form['#submit'];
  xmlsitemap_add_link_bundle_settings($form, $form_state, 'commerce_product', $type);
  $xmlsitemap_submit_handlers = array_diff($form['#submit'], $submit_handlers_before);

  // add submit handler to action buttons - @see commerce_product_ui_product_type_form
  _xmlsitemap_commerce_product_prepend_action_submit_handlers($form, $xmlsitemap_submit_handlers);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add the XML sitemap individual link options for a product.
 *
 * @see commerce_product_product_form()
 * @see xmlsitemap_add_form_link_options()
 */
function xmlsitemap_commerce_product_form_commerce_product_product_form_alter(&$form, $form_state) {
  module_load_include('inc', 'xmlsitemap', 'xmlsitemap.admin');
  $product = $form_state['commerce_product'];
  $product_id = !empty($product->product_id) ? $product->product_id : NULL;

  // add xmlsitemap link options
  $submit_handlers_before = $form['#submit'];
  xmlsitemap_add_form_link_options($form, 'commerce_product', $product->type, $product_id);
  $xmlsitemap_submit_handlers = array_diff($form['#submit'], $submit_handlers_before);

  // add our submit handler after xmlsitemap submit handlers
  $xmlsitemap_submit_handlers[] = '_xmlsitemap_commerce_product_form_commerce_product_product_form_submit';

  // add all submit handlers to action buttons - @see commerce_product_product_form
  // - if we used hook_field_attach_form() then we wouldn't have to do this, but
  //   commerce_product form has a bug (http://drupal.org/node/1279300)
  _xmlsitemap_commerce_product_prepend_action_submit_handlers($form, $xmlsitemap_submit_handlers);

  // adjust xmlsitemap form weight
  $form['xmlsitemap']['#weight'] = 30;
}

/**
 * Submit handler for commerce_product_product_form()
 * - add xmlsitemap to the product object
 */
function _xmlsitemap_commerce_product_form_commerce_product_product_form_submit($form, &$form_state) {
  if (isset($form_state['values']['xmlsitemap'])) {
    $product = &$form_state['commerce_product'];
    $product->xmlsitemap = $form_state['values']['xmlsitemap'];
  }
}

/**
 * Prepend new submit handlers to the actions submit handlers for product forms
 */
function _xmlsitemap_commerce_product_prepend_action_submit_handlers(&$form, $new_submit_handlers) {
  if (empty($new_submit_handlers) || !isset($form['actions']['submit'])) {
    return;
  }

  $have_save_continue = isset($form['actions']['save_continue']);

  // ensure submit
  if (!isset($form['actions']['submit']['#submit'])) {
    $form['actions']['submit']['#submit'] = array();
  }

  // loop reverse so can unshift in correct order
  foreach (array_reverse($new_submit_handlers) as $submit_handler) {
    // add to submit
    if (!in_array($submit_handler, $form['actions']['submit']['#submit'])) {
      array_unshift($form['actions']['submit']['#submit'], $submit_handler);
    }

    // optionally add to save and continue
    if ($have_save_continue && !in_array($submit_handler, $form['actions']['save_continue']['#submit'])) {
      array_unshift($form['actions']['save_continue']['#submit'], $submit_handler);
    }
  }
}

/**
 * Fetch all the timestamps for when a product was changed.
 *
 * @param $product
 *   A product object.
 * @return
 *   An array of UNIX timestamp integers.
 */
function xmlsitemap_commerce_product_get_timestamps(stdClass $product) {
  static $timestamps = array();

  if (empty($product->product_id)) {
    return array();
  }

  if (!isset($timestamps[$product->product_id])) {
    $timestamps[$product->product_id] = db_query("SELECT p.changed FROM {commerce_product} p WHERE p.product_id = :product_id", array(':product_id' => $product->product_id))->fetchCol();
  }

  return $timestamps[$product->product_id];
}

/**
 * Create a sitemap link from a product.
 *
 * The link will be saved as $product->xmlsitemap.
 *
 * @param $product
 *   A product object.
 */
function xmlsitemap_commerce_product_create_link(stdClass $product) {
  if (!isset($product->xmlsitemap) || !is_array($product->xmlsitemap)) {
    $product->xmlsitemap = array();
    if (!empty($product->product_id) && ($link = xmlsitemap_link_load('commerce_product', $product->product_id))) {
      $product->xmlsitemap = $link;
    }
  }

  $settings = xmlsitemap_link_bundle_load('commerce_product', $product->type);
  $uri = entity_uri('commerce_product', $product);

  $product->xmlsitemap += array(
    'type' => 'commerce_product',
    'id' => $product->product_id,
    'subtype' => $product->type,
    'status' => $settings['status'],
    'status_default' => $settings['status'],
    'status_override' => 0,
    'priority' => $settings['priority'],
    'priority_default' => $settings['priority'],
    'priority_override' => 0,
  );

  // Always recalculate changefreq and changecount.
  $product->xmlsitemap['changefreq'] = 0;
  $product->xmlsitemap['changecount'] = 0;
  $max_timestamp = 0;

  if (!empty($product->product_id)) {
    $timestamps = xmlsitemap_commerce_product_get_timestamps($product);
    $product->xmlsitemap['changefreq'] = xmlsitemap_calculate_changefreq($timestamps);
    $product->xmlsitemap['changecount'] = count($timestamps) - 1;
    if (!empty($timestamps)) {
      $max_timestamp = max($timestamps);
    }
  }

  // The following values must always be checked because they are volatile.
  $product->xmlsitemap['loc'] = $uri['path'];
  $product->xmlsitemap['lastmod'] = $max_timestamp;
  $product->xmlsitemap['access'] = !empty($uri['path']) && (strpos($uri['path'], 'admin/commerce/products/') !== 0) && xmlsitemap_commerce_product_view_access($product, drupal_anonymous_user());
  $product->xmlsitemap['language'] = isset($product->language) ? $product->language : LANGUAGE_NONE;

  return $product->xmlsitemap;
}

/**
 * Determine whether a user may view the specified product.
 *
 * @param $product
 *   The product object on which the operation is to be performed
 * @param $account
 *   Optional, a user object representing the user for whom the operation is to
 *   be performed. Determines access for a user other than the current user.
 * @return
 *   TRUE if the operation may be performed, FALSE otherwise.
 *
 * @see commerce_product_access()
 */
function xmlsitemap_commerce_product_view_access($product, $account = NULL) {
  global $user;

  $rights = &drupal_static(__FUNCTION__, array());
  $op = 'view';

  // exist if no product
  if (empty($product)) {
    return FALSE;
  }

  // if no user supplied, use the current user
  if (empty($account)) {
    $account = $user;
  }

  // check cache
  $cid = "{$account->uid}::{$product->product_id}";
  if (isset($rights[$cid])) {
    return $rights[$cid];
  }

  // check product access
  $rights[$cid] = commerce_product_access($op, $product, $account);

  return $rights[$cid];
}
